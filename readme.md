## EMTing Getting data from Collections.

To use this example you must have an EMTing user. Get one at [MobilityLabs](https://mobilitylabs.emtmadrid.es/en).

In the example, you can get data from "Área Madrid Central" collection.

Try another collection in "[MobilityLabs Collections](https://mobilitylabs.emtmadrid.es/en/portal/collections/)".